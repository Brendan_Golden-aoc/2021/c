//
// Created by Brendan on 14/01/2022.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


typedef struct {
    int success;
    int *numbers;
    int (*cards)[5][5];
    size_t len_numbers;
    size_t len_cards;
} Generator_04;

// turns the textfile into an array of numbers
Generator_04 generator_04(const char * filename){
    FILE* fh = fopen( filename, "r");

    Generator_04 tmp;
    tmp.success = 1;

    // set up the array for cards
    size_t length_array_cards = 100;
    size_t numbers_length_array = 100;

    // initialise all required varibles
    tmp.cards = malloc(length_array_cards * sizeof(int[5][5]));
    for(size_t i=0;i< length_array_cards;i++){
        for(size_t k=0;k<5; k++){
            for(size_t l=0;l<5; l++){
                tmp.cards[i][k][l] = 0;
            }
        }
    }
    tmp.numbers = malloc(numbers_length_array * sizeof(int));

    tmp.len_numbers = 0;
    tmp.len_cards = 0;

    //check if file exists
    if (fh == NULL){
        printf("file does not exists %s", filename);
        tmp.success = 0;
        fclose(fh);
        return tmp;
    }

    // start it off with 100
    size_t length_steps = 100;

    //read line by line
    const int line_size = 300;
    char* line = malloc(line_size);

    // first line is the numbers
    int bool_numbers = 0;
    int bool_card_active = 0;

    // line of the card we are currently looking at
    size_t card_line = 0;



    while (fgets(line, line_size, fh) != NULL)  {
        // deal with first line
        if(!bool_numbers){
            char *token;

            token = strtok(line,  ",");
            while(token) {
                if(tmp.len_numbers >= numbers_length_array){
                    // incriment array by length_steps
                    numbers_length_array += length_steps;

                    // make the array bigger
                    tmp.numbers = realloc(tmp.numbers, numbers_length_array * sizeof(int));
                }

                tmp.numbers[tmp.len_numbers] = (int) strtol(token, NULL, 10);

                // incriment for teh next round
                tmp.len_numbers += 1;

                // get teh next one for the loop
                token = strtok(NULL,  ",");
            }

            // make sure it does not run next time
            bool_numbers = 1;
            continue;
        }

        if(!bool_card_active){
            bool_card_active = 1;
            continue;
        }

        if(strlen(line) > 1){
            // check if the cards array is big enough
            if(tmp.len_cards >= length_array_cards){
                // incriment array by length_steps
                length_array_cards += length_steps;

                // make the array bigger
                tmp.cards = realloc(tmp.cards, length_array_cards * sizeof(int[5][5]));

                // make sure all teh data is properly initialised
                for(size_t i=(length_array_cards - length_steps);i< length_array_cards;i++){
                    for(int k=0;k<5; k++){
                        for(int l=0;l<5; l++){
                            tmp.cards[i][k][l] = 0;
                        }
                    }
                }
            }


            // thanks Manuel
            sscanf(line,"%d %d %d %d %d",
                     &tmp.cards[tmp.len_cards][card_line][0],
                     &tmp.cards[tmp.len_cards][card_line][1],
                     &tmp.cards[tmp.len_cards][card_line][2],
                     &tmp.cards[tmp.len_cards][card_line][3],
                     &tmp.cards[tmp.len_cards][card_line][4]
                     );


            /*
            char *token;
            char *next_token = NULL;

            size_t position = 0;

            token = strtok_s(line,  " ", &next_token);
            while(token) {
                tmp.cards[tmp.len_cards][card_line][position] = strtol(token, NULL, 10);

                // incriment for teh next round
                position += 1;

                // get teh next one for the loop
                token = strtok_s(NULL,  ",", &next_token);
            }
            */

            card_line += 1;

        } else {
            tmp.len_cards += 1;

            // reset teh card line
            card_line = 0;
        }
    }
    // to tidy up
    tmp.len_cards += 1;

    // free memory after use
    free(line);
    fclose(fh);

    // return both the length of the array and a pointer to it
    return tmp;
}





int part_1_04 (Generator_04* data){
    size_t min_turns = data->len_numbers + 1;
    int card_score = 0;

    // loop through teh cards, completing tehm
    for(int i=0;i<data->len_cards; i++){
        // for each card loop through the list of numbers

        // get sum of all numbers, each cards is 5x5
        int sum = 0;
        for(int k=0;k<5; k++){
            for(int l=0;l<5; l++){
                sum += data->cards[i][k][l];
            }
        }


        int cols[5] = {0};
        int rows[5] = {0};

        for(int j=0;j<data->len_numbers; j++){
            if(j >= min_turns){
                // continue the outer loop
                goto continue_outer;
            }


            int number = data->numbers[j];

            // if found remove teh number from teh sum

            // loop through the board
            for(int k=0;k<5; k++){
                for(int l=0;l<5; l++){
                    int cell = data->cards[i][k][l];
                    if (cell == number){
                        sum -= number;

                        cols[k] += 1;
                        rows[l] += 1;

                        // filled out a row or col
                        if(cols[k] == 5 || rows[l] == 5){
                            // set new min turns
                            min_turns = j;


                            //printf("card: %d   Sum: %d  number: %d\n", i, sum, number);


                            card_score = sum * number;
                        }
                    }
                }
            }
        }

        continue_outer:;
    }


    return card_score;
}



int part_2_04 (Generator_04* data){
    size_t turns = 0;
    int card_score = 0;

    // loop through teh cards, completing tehm
    for(int i=0;i<data->len_cards; i++){
        // for each card loop through the list of numbers

        // get sum of all numbers, each cards is 5x5
        int sum = 0;
        for(int k=0;k<5; k++){
            for(int l=0;l<5; l++){
                sum += data->cards[i][k][l];
            }
        }


        int cols[5] = {0};
        int rows[5] = {0};

        for(int j=0;j<data->len_numbers; j++){
            int number = data->numbers[j];

            // if found remove teh number from teh sum

            // loop through the board
            for(int k=0;k<5; k++){
                for(int l=0;l<5; l++){
                    int cell = data->cards[i][k][l];
                    if (cell == number){
                        sum -= number;

                        cols[k] += 1;
                        rows[l] += 1;

                        // filled out a row or col
                        if(cols[k] == 5 || rows[l] == 5){
                            if(j <= turns){
                                goto continue_outer;
                            }

                            // set new max turns
                            turns = j;

                            card_score = sum * number;

                            // this card is complete, go to next
                            goto continue_outer;
                        }
                    }
                }
            }
        }

        continue_outer:;
    }

    return card_score;
}


void day04(void){
    Generator_04 tmp = generator_04("../input/day04.txt");
    if(!tmp.success){return;}

    //printf("Numbers: %d  Cards: %d First cell first card: %d\n", tmp.len_numbers, tmp.len_cards, tmp.cards[0][0][0] );

    //int part_1 = 0;
    //int part_2 = 0;
    int part_1 = part_1_04(&tmp);
    int part_2 = part_2_04(&tmp);

    printf( "Day 04\n"
            "   Part 1: %d\n"
            "   Part 2: %d\n"
            "\n"
            , part_1, part_2);

    // clean up
    free(tmp.numbers);
    free(tmp.cards);
}