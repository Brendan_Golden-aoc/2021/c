//
// Created by Brendan on 14/01/2022.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


// https://stackoverflow.com/a/28609778/11964934
void removeChars(char *string, char c){
    int writer = 0, reader = 0;

    while (string[reader]){
        if (string[reader] != c){
            string[writer++] = string[reader];
        }

        reader++;
    }

    string[writer]=0;
}

// turns the textfile into an array of numbers
void * generator_03(const char * filename, size_t * length){
    FILE* fh = fopen( filename, "r");

    //check if file exists
    if (fh == NULL){
        printf("file does not exists %s", filename);
        fclose(fh);
        return NULL;
    }

    // start it off with 100
    size_t length_steps = 100;
    size_t length_array = 100;
    size_t lines = 0;

    // initialise the array with memory
    char **data = malloc(length_array * sizeof(char(*)));

    //read line by line
    const int line_size = 300;
    char* line = malloc(line_size);
    while (fgets(line, line_size, fh) != NULL)  {
        // keep track of lines/elements

        if(lines >= length_array){
            // incriment array by length_steps
            length_array += length_steps;

            // make the array bigger
            data = realloc(data, length_array * sizeof(char(*)));
        }

        // clean the string of teh newline
        //removeChars(line, '\n');

        // have toa llocate memory for teh indivual strings, not quite certain how this works though
        data[lines] = malloc(strlen(line)+1);
        strcpy(data[lines], line);

        lines += 1;
    }

    // free memory after use
    free(line);
    fclose(fh);

    // return both the length of the array and a pointer to it
    *length = lines;
    return data;
}

int part_1_03 (char **data, size_t length){
    // -1 is to deal witht eh newline at teh end
    size_t length_line = strlen(data[0]) - 1;

    //int counter[length_line];
    int *counter = malloc(length_line * sizeof(int));
    // set values to 0
    // https://stackoverflow.com/a/17937874/11964934
    memset (counter, 0, length_line * sizeof(int));

    for(int i=0; i<length;i++){
        for(int j=0;j< length_line; j++){
            if (data[i][j] == '1'){
                counter[j] += 1;
            }
        }
    }

    // not sure why +1 but it keeps some errors at bay
    char *gamma = malloc((length_line+1) * sizeof(char));
    char *epsilon = malloc((length_line+1) * sizeof(char));
    for(int j=0;j< length_line; j++){
        if (counter[j] > (length/2)){
            gamma[j] = '1';
            epsilon[j] = '0';
        } else {
            gamma[j] = '0';
            epsilon[j] = '1';
        }
    }

    int result = (int) strtol(gamma, NULL, 2)*(int) strtol(epsilon, NULL, 2);

    free(counter);
    free(gamma);
    free(epsilon);

    return result;
}

int general(char **data, size_t position, size_t length, int oxygen){
    //printf("position: %d   length: %d\n", position, length);


    // oversized I know but still
    char **tmp_0 = malloc(length * sizeof(char(*)));
    char **tmp_1 = malloc(length * sizeof(char(*)));

    //printf("  start loop");
    size_t length_0 = 0;
    size_t length_1 = 0;
    for(int i=0; i<length;i++){
        //printf("  i: %d\n", i);
        switch (data[i][position]) {
            case '0' : {
                tmp_0[length_0] = data[i];
                length_0 += 1;
                break;
            }
            case '1' : {
                tmp_1[length_1] = data[i];
                length_1 += 1;
                break;
            }
            default: break;
        }
    }

    //printf("  length_0: %d  length_1: %d\n", length_0, length_1);
    // o2   length_1 >= length_0
    // c02  length_1 < length_0

    // false
    int choice;
    if (oxygen){
        choice = length_1 >= length_0;
    } else {
        choice = length_1 < length_0;
    }

    if(position > 0){
        free(data);
    }
    if (choice){
        if(length_1 > 1){
            free(tmp_0);

            return general(tmp_1, position +1, length_1, oxygen);
        } else if (length_1 == 1){
            int result = (int) strtol(tmp_1[0], NULL, 2);
            free(tmp_0);
            free(tmp_1);
            return result;
        }
    } else {
        if(length_0 > 1){
            free(tmp_1);

            return general(tmp_0, position +1, length_0, oxygen);
        } else if (length_0 == 1){
            int result = (int) strtol(tmp_0[0], NULL, 2);
            free(tmp_0);
            free(tmp_1);
            return result;
        }
    }

    return 0;
}

int part_2_03 (char **data, size_t length){
    // 1 is oxygen, 0 is c02
    return general(data, 0, length, 1)
    * general(data, 0, length, 0);
}


void day03(void){
    size_t length;
    char **tmp = generator_03("../input/day03.txt", &length);
    if (tmp == NULL){
        free(tmp);
        return;
    }

    int part_1 = part_1_03(tmp, length);
    int part_2 = part_2_03(tmp, length);

    printf( "Day 03\n"
            "   Part 1: %d\n"
            "   Part 2: %d\n"
            "\n"
            , part_1, part_2);

    // clean up
    for(int i=0;i< length;i++){
        free(tmp[i]);
    }
    free(tmp);
}