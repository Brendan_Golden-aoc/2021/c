//
// Created by Brendan on 08/01/2022.
//

#include <stdio.h>
#include <stdlib.h>

// turns the textfile into an array of numbers
void * generator(const char * filename, size_t * length){
    FILE* fh = fopen( filename, "r");

    //check if file exists
    if (fh == NULL){
        printf("file does not exists %s", filename);
        fclose(fh);
        return NULL;
    }

    // start it off with 100
    size_t length_steps = 100;
    size_t length_array = 100;
    size_t lines = 0;

    // initialise the array with memory
    int *data = malloc(length_array * sizeof(int));

    //read line by line
    const int line_size = 300;
    char* line = malloc(line_size);
    while (fgets(line, line_size, fh) != NULL)  {
        // keep track of lines/elements

        if(lines >= length_array){
            // incriment array by length_steps
            length_array += length_steps;

            // make the array bigger
            int *re_allocated_array = realloc(data, length_array * sizeof(int));
            if (re_allocated_array) {
                data = re_allocated_array;
            } else {
                // deal with realloc failing because memory could not be allocated.
                // not dealt with....
            }
        }
        data[lines] = (int) strtol(line, NULL, 10);

        lines += 1;
    }

    // free memory after use
    free(line);
    fclose(fh);

    // return both the length of the array and a pointer to it
    *length = lines;
    return data;
}

void day01(void){
    size_t length;
    int *tmp = generator("../input/day01.txt", &length);
    if (tmp == NULL){
        free(tmp);
        return;
    }

    int part_1 = 0;
    int part_2 = 0;

    // Day one is pretty simple, literally going through the array checking the current with the previous
    for(int i=1; i < length; i++){
        if (tmp[i] > tmp[i-1]){
            part_1 += 1;
        }

        if(i > 2){
            if((tmp[i] + tmp[i-1] + tmp[i-2])>(tmp[i-1] + tmp[i-2] + tmp[i-3])){
                part_2 += 1;
            }
        }

    }
    printf( "Day 01\n"
            "   Part 1: %d\n"
            "   Part 2: %d\n"
            "\n"
            , part_1, part_2);

    // clean up
    free(tmp);
}