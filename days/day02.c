//
// Created by Brendan on 14/01/2022.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Commands_ {
    char  direction[10];
    long   units;
} Commands;

// turns the textfile into an array of numbers
void * generator_02(const char * filename, size_t * length){
    FILE* fh = fopen( filename, "r");

    //check if file exists
    if (fh == NULL){
        printf("file does not exists %s", filename);
        fclose(fh);
        return NULL;
    }

    // start it off with 100
    size_t length_steps = 100;
    size_t length_array = 100;
    size_t lines = 0;

    // initialise the array with memory
    Commands *data = malloc(length_array * sizeof(Commands));

    //read line by line
    const int line_size = 300;
    char* line = malloc(line_size);
    while (fgets(line, line_size, fh) != NULL)  {
        // keep track of lines/elements

        if(lines >= length_array){
            // incriment array by length_steps
            length_array += length_steps;

            // make the array bigger
            Commands *re_allocated_array = realloc(data, length_array * sizeof(Commands));
            if (re_allocated_array) {
                data = re_allocated_array;
            } else {
                // deal with realloc failing because memory could not be allocated.
                // not dealt with....
            }
        }


        Commands tmp;

        // first value is the type of command
        char * value = strtok(line, " ");
        strcpy( tmp.direction, value);

        // get the second value as str
        value = strtok(NULL, " ");
        // convert it to a numebr
        tmp.units = strtol(value, NULL, 10);

        data[lines] = tmp;

        lines += 1;
    }

    // free memory after use
    free(line);
    fclose(fh);

    // return both the length of the array and a pointer to it
    *length = lines;
    return data;
}

long part_1_02 (Commands *data, size_t length){
    long horizontal = 0;
    long depth = 0;
    for(int i=0; i<length;i++){
        Commands command = data[i];

        if(strcmp(command.direction,"forward") == 0){
            horizontal += command.units;
        } else if(strcmp(command.direction,"down") == 0){
            depth += command.units;
        } else if(strcmp(command.direction,"up") == 0){
            depth -= command.units;
        }
    }


    return horizontal * depth;
}

long part_2_02 (Commands *data, size_t length){
    long horizontal = 0;
    long depth = 0;
    long aim = 0;

    for(int i=0; i<length;i++){
        Commands command = data[i];

        if(strcmp(command.direction,"forward") == 0){
            horizontal += command.units;
            depth += aim * command.units;
        }
        if(strcmp(command.direction,"down") == 0){
            aim += command.units;
        }
        if(strcmp(command.direction,"up") == 0){
            aim -= command.units;
        }
    }


    return horizontal * depth;
}


void day02(void){
    size_t length;
    Commands *tmp = generator_02("../input/day02.txt", &length);
    if (tmp == NULL){
        free(tmp);
        return;
    }

    long part_1 = part_1_02(tmp, length);
    long part_2 = part_2_02(tmp, length);

    printf( "Day 02\n"
            "   Part 1: %ld\n"
            "   Part 2: %ld\n"
            "\n"
            , part_1, part_2);

    // clean up
    free(tmp);
}