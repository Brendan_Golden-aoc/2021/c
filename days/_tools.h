//
// Created by Brendan on 08/01/2022.
//

#ifndef C_GET_INPUT_H
#define C_GET_INPUT_H

#endif //C_GET_INPUT_H

void * get_input(const char * f_name, int * err2, size_t * f_size);