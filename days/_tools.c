//
// Created by Brendan on 08/01/2022.
//

#include <stdio.h>
#include <stdlib.h>

#define FILE_OK 0
#define FILE_NOT_EXIST 1
#define FILE_READ_ERROR 2

void * get_input(const char * f_name, int * err2, size_t * f_size) {
    FILE *stream = fopen( f_name, "rb");

    if (stream == NULL){
        printf( "The file '%s' was opened\n", f_name);
    }else{
        printf( "The file '%s' was not opened\n", f_name);

        return NULL;
    }

    char * buffer;
    size_t length;
    size_t read_length;

    if( stream ) {
        fseek(stream, 0, SEEK_END);
        length = ftell(stream);
        fseek(stream, 0, SEEK_SET);

        buffer = (char *)malloc(length + 1);

        if (length) {
            read_length = fread(buffer, 1, length, stream);

            if (length != read_length) {
                free(buffer);
                *err2 = FILE_READ_ERROR;

                return NULL;
            }
        }

        *err2 = FILE_OK;
        buffer[length] = '\0';
        *f_size = length;

        if ( fclose( stream ) != 0 ) {
            printf( "The file '%s' was not closed\n", f_name);
        }
    } else {
        *err2 = FILE_NOT_EXIST;

        return NULL;
    }

    return buffer;
}