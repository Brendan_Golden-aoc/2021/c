#include <stdio.h>

#include "./days/_days.h"

int main() {
    day01();
    day02();
    day03();
    day04();
    return 0;
}
